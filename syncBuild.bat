Set dest=C:\Falcon BMS 4.33 U1\Data\Add-On Israel1980s
Set src=D:\1980sgitlab\1980sIsrael\Add-On Israel1980s
@ECHO OFF

CLS
ECHO :::::::::::: WARNING ::::::::::::
ECHO :::::::::::: WARNING ::::::::::::
ECHO :::::::::::: WARNING ::::::::::::
ECHO Make sure you edit this file in notepad and change line 1-2 to your destination and source directories!
ECHO :::::::::::: WARNING ::::::::::::
ECHO :::::::::::: WARNING ::::::::::::
ECHO :::::::::::: WARNING ::::::::::::
ECHO 1.Sync Master (Gitlab)
ECHO.

CHOICE /C 1 /M "Enter your choice:"

IF ERRORLEVEL 1 GOTO Master
pause

:Master
GOTO End
pause

:End
robocopy "%src%" "%dest%" /MIR /Z /XA:H /W:5
ECHO ---
ECHO --- 
ECHO Sync complete
pause
